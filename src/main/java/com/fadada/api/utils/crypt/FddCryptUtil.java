package com.fadada.api.utils.crypt;

import com.fadada.api.utils.string.StringUtil;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;

public class FddCryptUtil {

    private final static Charset UTF8 = StandardCharsets.UTF_8;

    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

    public static byte[] hmac256(byte[] key, String msg) throws Exception {
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, mac.getAlgorithm());
        mac.init(secretKeySpec);
        return mac.doFinal(msg.getBytes(UTF8));
    }

    public static String sha256Hex(String s) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] d = md.digest(s.getBytes(UTF8));
        return DatatypeConverter.printHexBinary(d).toLowerCase();
    }

    /**
     * @param sortParam 排序后得参数字符串
     * @param timestamp 时间戳
     * @param appKey    appKey
     * @return
     * @throws Exception
     */
    public static String sign(String sortParam,
                              String timestamp, String appKey) throws Exception {
        //将排序后字符串转为sha256Hex
        String signText = sha256Hex(sortParam);
        // ************* 计算签名 *************
        byte[] secretSigning = hmac256((appKey).getBytes(UTF8), timestamp);
        //计算后得到签名
        return DatatypeConverter.printHexBinary(hmac256(secretSigning, signText)).toLowerCase();
    }


    public static String sortParameters(Map<String, String> parameters) {
        if (parameters.isEmpty()) {
            return null;
        }
        List<String> removeKeys = new ArrayList<>();
        for (String key : parameters.keySet()) {
            if (StringUtil.isBlank(parameters.get(key))) {
                removeKeys.add(key);
            }
        }
        for (String key : removeKeys) {
            parameters.remove(key);
        }
        StringBuffer sb = new StringBuffer();
        SortedMap<String, String> paramMap = new TreeMap<>(parameters);
        int index = 0;
        for (String key : paramMap.keySet()) {
            String value = paramMap.get(key);
            sb.append(key).append("=").append(value);
            index++;
            if (index != parameters.size()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }


    /**
     * AES 加密操作
     *
     * @param content   待加密内容
     * @param secretKey 加密密码
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String secretKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance(KEY_ALGORITHM);
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(secretKey.getBytes());
        kgen.init(128, random);
        SecretKey secretKeya = kgen.generateKey();
        byte[] enCodeFormat = secretKeya.getEncoded();
        SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, KEY_ALGORITHM);
        Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
        byte[] byteContent = content.getBytes(UTF8);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] result = cipher.doFinal(byteContent);
        return org.apache.commons.codec.binary.Base64.encodeBase64String(result);
    }

    /**
     * AES 解密操作
     *
     * @param content
     * @param secretKey
     * @return
     */
    public static String decrypt(String content, String secretKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance(KEY_ALGORITHM);
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(secretKey.getBytes());
        kgen.init(128, random);
        SecretKey secretKeya = kgen.generateKey();
        byte[] enCodeFormat = secretKeya.getEncoded();
        SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, KEY_ALGORITHM);
        Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] result = cipher.doFinal(Base64.decodeBase64(content));
        return new String(result, UTF8);
    }
}

