package com.fadada.api.utils.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * 文件工具类
 */
public class FileUtil {

    public static Logger log = LoggerFactory.getLogger(FileUtil.class);

    /**
     * 文件写入
     *
     * @param fileBytes
     * @param path
     * @param fileName
     */
    public static void fileSink(byte[] fileBytes, String path, String fileName) {
        File f = new File(path + fileName);
        if (f.exists()) {
            f.delete();
        }
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(f);
             BufferedOutputStream bw = new BufferedOutputStream(fos)) {
            bw.write(fileBytes);
        } catch (Exception e) {
            log.error("文件写入失败：{}", e);
        }
    }
}
