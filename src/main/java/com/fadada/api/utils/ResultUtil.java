package com.fadada.api.utils;


import com.fadada.api.bean.rsp.BaseRsp;
import com.fadada.api.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yh
 * @version 1.0.0
 * @ClassName ResultUtil.java
 * @Description 结果校验工具
 * @Param
 * @createTime 2020年07月08日 17:28:00
 */
public class ResultUtil {

    public static Logger log = LoggerFactory.getLogger(ResultUtil.class);

    private ResultUtil() {
    }

    /**
     * 请求结果校验抛出异常 并打日志
     *
     * @param baseRsp
     * @throws ApiException
     */
    public static void checkResult(BaseRsp baseRsp) throws ApiException {
        if (baseRsp == null || !Boolean.TRUE.equals(baseRsp.isSuccess())) {
            log.error("请求失败：{}", baseRsp);
            throw new ApiException(baseRsp.getMsg());
        }
        log.info("请求结果为：{}", baseRsp);
    }

}
