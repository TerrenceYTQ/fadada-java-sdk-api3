package com.fadada.api.bean.rsp.sign;

import com.fadada.api.bean.BaseBean;

import java.util.List;

public class GetSignTaskDownloadUrlsRsp extends BaseBean {
    private String downloadZipUrl;

    private List<DownloadUrlInfo> downloadUrls;

    public static class DownloadUrlInfo extends BaseBean {
        private String fileId;
        private String fileName;
        private String downloadUrl;
        private Integer fileType;

        public String getFileId() {
            return fileId;
        }

        public void setFileId(String fileId) {
            this.fileId = fileId;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getDownloadUrl() {
            return downloadUrl;
        }

        public void setDownloadUrl(String downloadUrl) {
            this.downloadUrl = downloadUrl;
        }

        public Integer getFileType() {
            return fileType;
        }

        public void setFileType(Integer fileType) {
            this.fileType = fileType;
        }
    }

    public String getDownloadZipUrl() {
        return downloadZipUrl;
    }

    public void setDownloadZipUrl(String downloadZipUrl) {
        this.downloadZipUrl = downloadZipUrl;
    }

    public List<DownloadUrlInfo> getDownloadUrls() {
        return downloadUrls;
    }

    public void setDownloadUrls(List<DownloadUrlInfo> downloadUrls) {
        this.downloadUrls = downloadUrls;
    }
}
