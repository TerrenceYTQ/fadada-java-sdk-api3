package com.fadada.api.bean.rsp.account;

import com.fadada.api.bean.BaseBean;

/**
 * @Author Fadada
 * @Date 2021/8/10 14:05:58
 * @Version 3.5
 */
public class GetPersonAccountStatusRsp extends BaseBean {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
