package com.fadada.api.bean.rsp.template;

import com.fadada.api.bean.BaseBean;

import java.util.List;

/**
 * @author yh128
 * @version 1.4.0
 * @ClassName AddWidgetsRsp.java
 * @Description 批量新增自定义控件响应
 * @Param
 * @createTime 2021年06月17日 17:48:00
 */
public class AddWidgetsRsp extends BaseBean {
    private List<FailWidgetInfo> failWidgetList;

    public static class FailWidgetInfo extends BaseBean {
        private String widgetName;

        private String failReason;

        public String getWidgetName() {
            return widgetName;
        }

        public void setWidgetName(String widgetName) {
            this.widgetName = widgetName;
        }

        public String getFailReason() {
            return failReason;
        }

        public void setFailReason(String failReason) {
            this.failReason = failReason;
        }
    }

    public List<FailWidgetInfo> getFailWidgetList() {
        return failWidgetList;
    }

    public void setFailWidgetList(List<FailWidgetInfo> failWidgetList) {
        this.failWidgetList = failWidgetList;
    }
}
