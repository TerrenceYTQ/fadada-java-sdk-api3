package com.fadada.api.bean.rsp.revise;

import com.fadada.api.bean.BaseBean;

/**
 * @author yh128
 * @version 1.0.0
 * @ClassName GetReviseTeskPreviewUrlRsp.java
 * @Description 获取填充地址请求对象
 * @Param
 * @createTime 2020年11月25日 15:55:00
 */
public class GetReviseTeskPreviewUrlRsp extends BaseBean {
    private String previewUrl;

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }
}
