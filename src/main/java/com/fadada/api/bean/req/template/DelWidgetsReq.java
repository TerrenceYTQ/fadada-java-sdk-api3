package com.fadada.api.bean.req.template;

import com.fadada.api.bean.req.BaseReq;

import java.util.List;

/**
 * @author yh128
 * @version 1.4.0
 * @className DelWidgetsReq
 * @description 批量删除自定义控件
 * @createTime 2021年6月13日 15:27:43
 */
public class DelWidgetsReq extends BaseReq {
    private Integer delWay;

    private List<DelWidgetInfo> widgetList;

    public static class DelWidgetInfo {
        private String widgetName;

        public String getWidgetName() {
            return widgetName;
        }

        public void setWidgetName(String widgetName) {
            this.widgetName = widgetName;
        }
    }

    public Integer getDelWay() {
        return delWay;
    }

    public void setDelWay(Integer delWay) {
        this.delWay = delWay;
    }

    public List<DelWidgetInfo> getWidgetList() {
        return widgetList;
    }

    public void setWidgetList(List<DelWidgetInfo> widgetList) {
        this.widgetList = widgetList;
    }
}
