package com.fadada.api.bean.req.template;

import com.fadada.api.annotation.ParamsVerif;
import com.fadada.api.bean.BaseBean;
import com.fadada.api.bean.req.BaseReq;

import java.util.List;

/**
 * @author yh128
 * @version 1.4.0
 * @ClassName AddWidgetsReq.java
 * @Description 批量新增控件请求对象
 * @Param
 * @createTime 2021年06月05日 10:37:00
 */
@ParamsVerif
public class AddWidgetsReq extends BaseReq {
    private List<AddWidgetsInfo> widgetList;

    public static class AddWidgetsInfo extends BaseBean {
        private Integer widgetType;

        private String widgetName;

        private Integer isRequired;

        private WidgetAttribute widgetAttribute;

        public Integer getWidgetType() {
            return widgetType;
        }

        public void setWidgetType(Integer widgetType) {
            this.widgetType = widgetType;
        }

        public String getWidgetName() {
            return widgetName;
        }

        public void setWidgetName(String widgetName) {
            this.widgetName = widgetName;
        }

        public Integer getIsRequired() {
            return isRequired;
        }

        public void setIsRequired(Integer isRequired) {
            this.isRequired = isRequired;
        }

        public WidgetAttribute getWidgetAttribute() {
            return widgetAttribute;
        }

        public void setWidgetAttribute(WidgetAttribute widgetAttribute) {
            this.widgetAttribute = widgetAttribute;
        }
    }

    public static class WidgetAttribute extends BaseBean {
        private String widgetValue;

        private Integer align;

        private Integer fontType;

        private Integer fontSize;

        public String getWidgetValue() {
            return widgetValue;
        }

        public void setWidgetValue(String widgetValue) {
            this.widgetValue = widgetValue;
        }

        public Integer getAlign() {
            return align;
        }

        public void setAlign(Integer align) {
            this.align = align;
        }

        public Integer getFontType() {
            return fontType;
        }

        public void setFontType(Integer fontType) {
            this.fontType = fontType;
        }

        public Integer getFontSize() {
            return fontSize;
        }

        public void setFontSize(Integer fontSize) {
            this.fontSize = fontSize;
        }
    }

    public List<AddWidgetsInfo> getWidgetList() {
        return widgetList;
    }

    public void setWidgetList(List<AddWidgetsInfo> widgetList) {
        this.widgetList = widgetList;
    }
}


