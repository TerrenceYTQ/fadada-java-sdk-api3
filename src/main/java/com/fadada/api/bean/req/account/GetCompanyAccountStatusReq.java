package com.fadada.api.bean.req.account;

import com.fadada.api.bean.req.BaseReq;

/**
 * @Author Fadada
 * @Date 2021/8/10 14:05:58
 * @Version 3.5
 */
public class GetCompanyAccountStatusReq extends BaseReq {
    private String companyName;
    private String contactPersonName;
    private String contactPersonAddress;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonAddress() {
        return contactPersonAddress;
    }

    public void setContactPersonAddress(String contactPersonAddress) {
        this.contactPersonAddress = contactPersonAddress;
    }
}
