package com.fadada.api.bean.req.sign;

import com.fadada.api.bean.BaseBean;
import com.fadada.api.bean.req.BaseReq;

import java.util.List;


public class GetSignTaskDownloadUrlsReq extends BaseReq {

    private String taskId;

    private List<DownLoadFileInfo> files;

    public static class DownLoadFileInfo extends BaseBean {
        private String fileId;

        public String getFileId() {
            return fileId;
        }

        public void setFileId(String fileId) {
            this.fileId = fileId;
        }
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public List<DownLoadFileInfo> getFiles() {
        return files;
    }

    public void setFiles(List<DownLoadFileInfo> files) {
        this.files = files;
    }
}
