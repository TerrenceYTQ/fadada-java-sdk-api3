package com.fadada.api.bean.req.account;

import com.fadada.api.bean.req.BaseReq;

/**
 * @Author Fadada
 * @Date 2021/8/10 14:05:58
 * @Version 3.5
 */
public class GetPersonAccountStatusReq extends BaseReq {
    private String personName;
    private String contactAddress;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }
}
